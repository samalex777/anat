import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardonePageRoutingModule } from './cardone-routing.module';

import { CardonePage } from './cardone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardonePageRoutingModule
  ],
  declarations: [CardonePage]
})
export class CardonePageModule {}
