import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CardonePage } from './cardone.page';

describe('CardonePage', () => {
  let component: CardonePage;
  let fixture: ComponentFixture<CardonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CardonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
