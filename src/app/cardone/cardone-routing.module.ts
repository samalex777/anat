import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardonePage } from './cardone.page';

const routes: Routes = [
  {
    path: '',
    component: CardonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardonePageRoutingModule {}
